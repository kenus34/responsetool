package com.pgplus.platformresponse.platformresponse;

public final class Constant {
    public static final String RESOURCEFILE = "facade_cmd.properties";
    public static final String BLACKLISTED = "is.api.blacklisted";
    public static final String ALIPAY_BASE_URL = "alipay.service.base.url";
    public static final String SUCCESS = "Successfully retrieved alipay response";
    public static final String FAILURE = "Failed to retrieve alipay response";
}
