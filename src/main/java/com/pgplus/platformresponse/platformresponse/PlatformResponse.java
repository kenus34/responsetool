package com.pgplus.platformresponse.platformresponse;

import com.paytm.pgplus.common.signature.wrapper.SignatureUtilWrapper;
import com.paytm.pgplus.facade.ControlCenter;
import com.paytm.pgplus.facade.common.model.AlipayRequest;
import com.paytm.pgplus.facade.utils.AlipayApiClient;
import com.paytm.pgplus.httpclient.config.ClientContextContainer;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.security.GeneralSecurityException;
import java.util.Properties;

public class PlatformResponse {
    private static Properties props;
    private static String[] blockedApis;
    private static  String ALIPAY_SERVICE_BASE_URL;
    private static final Logger LOGGER = LoggerFactory.getLogger(PlatformResponse.class);
    protected static void initialize() {
        LOGGER.info("Initializing client context.");
        ClientContextContainer.initialize();
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        props = new Properties();
        InputStream resourceStream = loader.getResourceAsStream(Constant.RESOURCEFILE);
        try {
            LOGGER.info("Loading resource files.");
            props.load(resourceStream);
        } catch (IOException e) {
            LOGGER.info("Failed to load properties.");
            e.printStackTrace();
        }
        LOGGER.info("Loading blacklisted API list.");
        blockedApis = props.getProperty(Constant.BLACKLISTED).split(";");
        LOGGER.info("Loading Alipay base url.");
        ALIPAY_SERVICE_BASE_URL = props.getProperty(Constant.ALIPAY_BASE_URL);
    }
    protected static boolean getPlatformResponse(String api,String jsonRequest){
        if(isBlacklisted(api)){
            LOGGER.info("Not allowed to process {}",api);
            return false;
        }
        String apiUrl = getUrl(api);
        try {
            LOGGER.info("Input json is {}",jsonRequest);
            final AlipayRequest alipayRequest = buildRequest(jsonRequest);
            LOGGER.info("Payload after addition of signature is {}",alipayRequest);
            final String alipayResponse = AlipayApiClient.sendPostRequestToAlipay(apiUrl, alipayRequest);
            LOGGER.info("Alipay Response json is {}",alipayResponse);
            return true;
        } catch (IOException e) {
            LOGGER.error("I/O exception occured ", e);
        } catch (Exception e) {
            LOGGER.error("Exception occured ", e);
        }

        return false;
    }

    protected static boolean filePlatformResponse(String api, String filePath){
        String jsonRequest=readRequestJson(filePath);
        return getPlatformResponse(api,jsonRequest);
    }

    private static AlipayRequest  buildRequest(String jsonString) throws IOException,GeneralSecurityException {

        try {
            String signature = SignatureUtilWrapper.signApiRequest(jsonString);
            StringBuilder requestBuilder = new StringBuilder();
            requestBuilder.append("{\"request\":").append(jsonString).append(",\"signature\":\"").append(signature)
                    .append("\"}");
            final AlipayRequest alipayRequest = new AlipayRequest(requestBuilder.toString());
            return alipayRequest;
        } catch (IOException| GeneralSecurityException e) {
            LOGGER.error("Failed to sign the json ",e);
            return null;
        }
    }

    private static boolean isBlacklisted(String api){
        for(String blockedApi:blockedApis){
            if(blockedApi.trim().equalsIgnoreCase(api.trim())){
                return true;
            }
        }
        return false;
    }

    private static String getUrl(String api){
        if (StringUtils.isEmpty(ALIPAY_SERVICE_BASE_URL)) {
            ControlCenter.stop("alipay.service.base.url is not defined in properties file. System will terminate now.");
        }
        else {
            StringBuilder alipayUrl = new StringBuilder()
                    .append(ALIPAY_SERVICE_BASE_URL)
                    .append(api);
            return alipayUrl.toString();
        }
        return null;
    }

    private static String readRequestJson(String jsonPath){
        LOGGER.info("Loading file {}",jsonPath);
        File file = new File(jsonPath);
        String st="";
        StringBuilder json=new StringBuilder();
        try{
        BufferedReader br = new BufferedReader(new FileReader(file));
        while ((st = br.readLine()) != null) {
            json.append(st);
        }
        }catch (IOException e){
            LOGGER.error("Failed to read {}",jsonPath);
        }
        return json.toString();
    }

}