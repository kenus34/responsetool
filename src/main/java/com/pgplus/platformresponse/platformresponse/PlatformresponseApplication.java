package com.pgplus.platformresponse.platformresponse;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;

@SpringBootApplication
public class PlatformresponseApplication {

    public static void main(String[] args) {
        PlatformResponse.initialize();
        SpringApplication.run(PlatformresponseApplication.class, args);
    }

}
@ShellComponent
class AliShellCommands {
    @ShellMethod(value = "Returns the response from platform for the given API and Json request")
    public String getResponse(String api,String jsonRequest) {
        return PlatformResponse.getPlatformResponse(api,jsonRequest)?Constant.SUCCESS:Constant.FAILURE;
    }

    @ShellMethod(value = "Returns the response from platform for the given API and Json request in the file")
    public String getResponseV2(String api,String filePath) {
        return PlatformResponse.filePlatformResponse(api,filePath)?Constant.SUCCESS:Constant.FAILURE;
    }
}
